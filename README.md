# Stacked Connections

ES6 d3.js stacked column chart / connection hybrid visualization.

## Install

```bash
# install package
npm install @lgv/stacked-connections
```

## Data Format

The following values are the expected input data structure; id is optional. `series.` keys are arbitrary; however, they must match whatever values provided to `series` during initialization.

```json
[
    {
        "id": 1,
        "series1": 8,
        "series2": 6,
        "series3": 4
    },
    {
        "id": 2,
        "series1": 1,
        "series2": 2,
        "series3": 3
    }
]
```

## Use Module

```bash
import { StackedConnections } from "@lgv/stacked-connections";

// have some data
let data = [
    {
        "id": 1,
        "series1": 8,
        "series2": 6,
        "series3": 4
    },
    {
        "id": 2,
        "series1": 1,
        "series2": 2,
        "series3": 3
    }
];

// initialize
const sc = new StackedConnections(data);

// render visualization
sc.render(document.body);
```

## Environment Variables

The following values can be set via environment or passed into the class.

| Name | Type | Description |
| :-- | :-- | :-- |
| `LGV_HEIGHT` | integer | height of artboard |
| `LGV_WIDTH` | integer | width of artboard |

## Events

The following events are dispatched from the svg element. Hover events toggle a class named `active` on the element.

| Target | Name | Event |
| :-- | :-- | :-- |
| column rect | `column-click` | on click |
| column rect | `column-mouseover` | on hover |
| column rect | `column-mousemout` | on un-hover |

## Style

Style is expected to be addressed via css. Any style not met by the visualization module is expected to be added by the importing component.

| Class | Element |
| :-- | :-- |
| `lgv-connections` | top-level svg element |
| `lgv-content` | content inside artboard inside padding |
| `lgv-column` | column chart column |
| `lgv-column-label` | column chart column label |
| `lgv-connection` | curved path |
| `lgv-series` | column chart series |

## Actively Develop

```bash
# clone repository
git clone <repo_url>

# update directory context
cd stacked-connections

# run docker container
docker run \
  --rm \
  -it  \
  -v $(pwd):/project \
  -w /project \
  -p 8080:8080 \
  node \
  bash

# FROM INSIDE RUNNING CONTAINER

# install module
npm install .

# run development server
npm run startdocker

# edit src/index.js
# add const sc = new StackedConnections(data);
# add sc.render(document.body);
# replace `data` with whatever data you want to develop with

# view visualization in browser at http://localhost:8080
```
