import test from "ava";

import { configurationLayout } from "@lgv/visualization-chart";
import { configuration } from "../src/configuration.js";
import { StackedConnections } from "../src/index.js";

/******************** EMPTY VARIABLES ********************/

// initialize
/*let sc = new StackedConnections();

// TEST INIT //
test("init", t => {

    t.true(sc.height === configurationLayout.height);
    t.true(sc.width === configurationLayout.width);

});

// TEST RENDER //
// have to turn off until d3.transition bug fixed
/*test("render", t => {

    // clear document
    document.body.innerHTML = "";

    // render to dom
    sbc.render(document.body);

    // get generated element
    let artboard = document.querySelector(`.${configuration.name}`);

    t.true(artboard !== undefined);
    t.true(artboard.nodeName == "svg");
    t.true(artboard.getAttribute("viewBox").split(" ")[3] == configurationLayout.height);
    t.true(artboard.getAttribute("viewBox").split(" ")[2] == configurationLayout.width);

});*/

/******************** DECLARED PARAMS ********************/

let testWidth = 300;
let testHeight = 500;
let testData = [...Array(50).keys()].map(d => ({
    id: `id-${d + 1}`,
    a: `a-${Math.floor((Math.random() * 7) + 1)}`,
    b: `b-${Math.floor((Math.random() * 10) + 1)}`,
    c: `c-${Math.floor((Math.random() * 20) + 1)}`,
    d: `d-${Math.floor((Math.random() * 50) + 1)}`
}));

// initialize
let scp = new StackedConnections(testData, testWidth, testHeight);

// TEST INIT //
test("init_params", t => {

    t.true(scp.height === testHeight);
    t.true(scp.width === testWidth);

});

// TEST RENDER //
// have to turn off until d3.transition bug fixed
/*test("render_params", t => {

    // clear document
    document.body.innerHTML = "";

    // render to dom
    sbcp.render(document.body);

    // get generated element
    let artboard = document.querySelector(`.${configuration.name}`);

    t.true(artboard !== undefined);
    t.true(artboard.nodeName == "svg");
    t.true(artboard.getAttribute("viewBox").split(" ")[3] == testHeight);
    t.true(artboard.getAttribute("viewBox").split(" ")[2] == testWidth);

});*/
