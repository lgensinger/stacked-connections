import { StackedColumnChart } from "@lgv/stacked-column-chart";
import { select, selectAll } from "d3-selection";

import { configuration, configurationLayout } from "../configuration.js";

/**
 * SCC is a series visualization.
 * @param {array} data - objects where each represents a series in the collection
 * @param {integer} height - artboard height
 * @param {integer} width - artboard width
 */
class SCC extends StackedColumnChart {
    constructor(data, width=configurationLayout.width, height=configurationLayout.height, seriesLabels=configurationLayout.seriesLabels, paddingSeries=configurationLayout.seriesPadding, normalize=configurationLayout.normailze, labelsOnTop=configurationLayout.labelsOnTop) {

        // initialize inheritance
        super(data, width, height, null, seriesLabels, paddingSeries, normalize, labelsOnTop);

    }

    /**
    * Declare Column mouse events.
    */
   configureColumnEvents() {
       this.column
           .on("click", (e,d) => {

               // clear connection classes
               selectAll(`.${this.prefix}-connection`).attr("class", `${this.prefix}-connection`);

               // update connection class
               selectAll(`.${this.prefix}-connection`)
                   .filter(x => x.full.includes(`.${d.key}.`))
                   .attr("class", `${this.prefix}-connection adjacent`);

               // send event to parent
               this.configureEvent("column-click",d,e);

           })
           .on("mouseover", (e,d) => {

               // update column class
               select(e.target).attr("class", `${this.classColumn} active`);

               // update connection class
               selectAll(`.${this.prefix}-connection:not(.adjacent)`)
                   .filter(x => x.full.includes(`.${d.key}.`))
                   .attr("class", d => `${this.prefix}-connection active`);

               // send event to parent
               this.configureEvent("column-mouseover",d,e);

           })
           .on("mouseout", (e,d) => {

               // update class
               select(e.target).attr("class", this.classColumn);

               // update connection class
               selectAll(`.${this.prefix}-connection:not(.adjacent)`).attr("class", `${this.prefix}-connection`);

               // send event to parent
               this.artboard.dispatch("column-mouseout", {
                   bubbles: true
               });

           });
   }


};


export { SCC };
export default SCC;
