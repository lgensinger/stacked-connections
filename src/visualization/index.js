import { path } from "d3-path";
import { select } from "d3-selection";

import { ChartLabel, LinearGrid } from "@lgv/visualization-chart";

import { configuration, configurationLayout, configurationSeries } from "../configuration.js";
import { StackConnectionData as SCD } from "../structure/stack-connections.js";
import { SCC } from "./stacked-column-chart.js";

/**
 * StackedConnections is a hybrid visualization of a series of stacked column charts with curved connection paths between related stacked values.
 * @param {array} data - objects where each represents a path in the hierarchy
 * @param {integer} height - artboard height
 * @param {integer} width - artboard width
 */
class StackedConnections extends LinearGrid {
    constructor(data, width=configurationLayout.width, height=configurationLayout.height, StackConnectionData=null, series=null,  seriesLabels=configurationSeries.useLabels, paddingSeries=configurationSeries.padding, normalize=configurationSeries.normalize, labelsOnTop=configurationSeries.labelsOnTop, name=configuration.name, label=configuration.branding) {

        // initialize inheritance
        super(data, width, height, StackConnectionData ? StackConnectionData : new SCD(data, { height: height, normalize: normalize, paddingSeries: paddingSeries, series: series, useLabels: seriesLabels, width: width }), label, name);

        // update self
        this.classConnection = `${label}-connection`;
        this.connection = null;

    }

    /**
     * Calculate column coordinates data from series key and column key.
     * @param {string} key - column key
     * @param {string} series - series key
     * @returns An object representing a column in a series.
     */
    calculateColumn(series, key) {
        return this.scc.data
            .filter(x => x.series == series)[0].values
            .filter(x => x.key == key)[0];
    }

    /**
     * Calculate bezier curve between source and target shapes.
     * @param {object} coordiantes - source/target with corresponding array of values of series, column, x, y0, y1
     * @returns A string representing the path in SVG path syntax.
     */
    calculateCurve(coordiantes) {

        let source = coordiantes.source;
        let target = coordiantes.target;

        // define connection path
        let p = path();

        // source top/left point of entire path shape
        p.moveTo(source.x, source.y0);

        // source top/left point cuved with 2 anchor points to top/right point of entire path shape
        p.bezierCurveTo(
            source.x + (this.scc.seriesScale.step() / 2), source.y0,
            target.x - (this.scc.seriesScale.step() / 2), target.y0,
            target.x, target.y0
        );
        // target straight line down the height of the stack item to bottom right point of entire path
        p.lineTo(target.x, target.y1);

        // target bottom/right point curved with 2 anchor points to bottom/left point of entire path shape
        p.bezierCurveTo(
            target.x - (this.scc.seriesScale.step() / 2), target.y0,
            source.x + (this.scc.seriesScale.step() / 2), source.y1,
            source.x, source.y1
        );

        // source bottom/left point straight up the height of the stack item to top/left point of entire path
        p.closePath();

        return p;

    }

    /**
     * Position and minimally style connections in SVG dom element.
     */
    configureConnections() {
        this.connection
            .transition().duration(1000)
            .attr("class", this.classConnection)
            .attr("data-id", d => d.full)
            .attr("data-label", d => `${d.source}-${d.target}`)
            .attr("fill", "lightgrey")
            .attr("opacity", 0.5)
            .attr("d", d => this.calculateCurve(this.determineCurveCoordiantes(d)))
            .attr("transform", `translate(0, ${this.scc.seriesLabelSpace})`)
    }

    /**
     * Determine points for generating curve between series' stacks.
     * @param {object} d - source/target keys with corresponding ids representing items in stack series
     * @returns An object of source/target with corresponding array of values of series, column, x, y0, y1.
     */
    determineCurveCoordiantes(d) {

        // filter for source/target series
        let sourceSeries = this.Data.extractSeries(d.source);
        let targetSeries = this.Data.extractSeries(d.target);

        // filter for source/target columns
        let sourceColumn = this.calculateColumn(sourceSeries, d.source);
        let targetColumn = this.calculateColumn(targetSeries, d.target);

        // source coordinates
        let sourceX = this.scc.seriesScale(sourceSeries) + this.scc.columnWidth(sourceColumn);
        let sourceY0 = this.scc.columnY(sourceColumn);
        let sourceY1 = sourceY0 + this.scc.columnHeight(sourceColumn);

        // target coordinates
        let targetX = this.scc.seriesScale(targetSeries) + this.scc.seriesScale.bandwidth() - this.scc.columnWidth(targetColumn);
        let targetY0 = this.scc.columnY(targetColumn);
        let targetY1 = targetY0 + this.scc.columnHeight(targetColumn);

        return {
            source: { x: sourceX, y0: sourceY0, y1: sourceY1 },
            target: { x: targetX, y0: targetY0, y1: targetY1 }
        }

    }

    /**
     * Generate visualization.
     */
    generateChart() {

        // initialize label
        this.Label = new ChartLabel(this.artboard, this.Data);

        // initialize stacked columns
        this.scc = new SCC(this.data.stacks, this.width, this.height, this.Data.useLabels, this.Data.padding, this.Data.normailze, this.Data.labelsOnTop);

        // update column chart configuration
        this.scc.artboard = this.artboard;
        this.scc.content = this.content;
        this.scc.Label = this.Label;

        // generate connection paths so behind labels from columns
        this.connection = this.generateConnections(this.content);
        this.configureConnections();

        // generate stacked columns
        this.scc.generateChart();
        this.scc.configureOverwrites();

    }

    /**
     * Construct connection paths in series in HTML DOM.
     * @param {node} domNode - HTML node
     * @returns A d3.js selection.
     */
    generateConnections(domNode) {
        return domNode
            .selectAll(`.${this.classConnection}`)
            .data(this.data.connections)
            .join(
                enter => enter.append("path"),
                update => update,
                exit => exit.transition()
                    .duration(1000)
                    .attr("transform", "scale(0)")
                    .remove()
            );
    }

    /**
     * Update visualization.
     * @param {object} data - key/values where each key is a
      series label and corresponding value is an array of values
      * @param {float} height - specified height of chart
      * @param {float} width - specified width of chart
     */
    update(data, width, height, series) {

        // update self
        this.heightSpecified = height || this.heightSpecified;
        this.widthSpecified = width || this.widthSpecified;

        // if layout data object
        if (this.Data.height && this.Data.width) {

            // update layout attributes
            this.Data.height = this.heightSpecified;
            this.Data.width = this.widthSpecified;

        }

        // recalculate values
        this.Data.source = data;
        this.Data.series = series;
        this.Data.data = this.Data.update;
        this.data = this.Data.data;

        // generate visualization
        this.generateVisualization();

    }

};

export { StackedConnections };
export default StackedConnections;
