import { sum } from "d3-array";

import { ChartData } from "@lgv/visualization-chart";

import { configurationSeries } from "../configuration.js";

/**
 * StackConnectionData is a data abstraction.
 * @param {object} data - usually array; occasionally object
 * @param {object} params - key/value pairs needed for getters
 */
class StackConnectionData extends ChartData
{
    constructor(data, params) {

        // initialize inheritance
        super(data, params);

        // update self
        this.labelsOnTop = params.labelsOnTop != undefined ? params.labelsOnTop : configurationSeries.labelsOnTop;
        this.normalize = params.normalize != undefined ? params.normalize : configurationSeries.normalize;
        this.padding = params.paddingSeries ? params.paddingSeries : configurationSeries.padding;
        this.series = params.series ? params.series : Object.keys(data[0]).filter(d => d != "id");
        this.useLabels = params.useLabels != undefined ? params.useLabels : configurationSeries.useLabels;
        this.data = this.condition(data, params);

    }

    /**
     * Condition data for visualization requirements.
     * @param {object} data - usually array; occasionally object
     * @returns An object or array of data specifically formatted to the visualization type.
     */
    condition(data, params) {

        let stacks = {};
        let connections = [];
        let tracked = [];

        if (this.series != undefined) {

            let useKey = !Array.isArray(this.series);

            // determine series array
            let series =  useKey ? this.series.series : this.series;

            // loop through series labels
            series.forEach((s,i) => {

                // generate empty object for each series
                stacks[s] = {};

                // sum values for series' columns
                [...new Set(data.map(d => d[s]))].forEach(d => stacks[s][d] = sum(data.filter(x => x[s] == d), y => useKey ? y[this.series.key] : 1));

                if (i < series.length - 1) {

                    // generate connections if not last in series
                    data.forEach(d => {

                        let source = d[s];
                        let target = d[series[i + 1]];
                        let full = `.${series.map(x => d[x]).join(".")}.`;
                        let id = `${source}.${target}.${full}`;

                        // check if already captured
                        if (!tracked.includes(id)) {

                            // add connections
                            connections.push({ source: source, target: target, full: full });

                            // track id
                            tracked.push(id);

                        }

                    });

                }

            });

        }

        return { stacks: stacks, connections: connections };
    }

    /**
     * Get series name from data.
     * @param {l} string - series column label
     * @returns A string representing a series name.
     */
    extractSeries(l) {
        return Object.keys(this.data.stacks).filter(s => Object.keys(this.data.stacks[s]).includes(l.toString()))[0];
    }

}

export { StackConnectionData };
export default StackConnectionData;
