import { configuration as config, configurationLayout as configLayout, configurationSeries as configSeries } from "@lgv/visualization-chart";
import packagejson from "../package.json";

const configuration = {
    branding: config.branding,
    name: packagejson.name.replace("/", "-").slice(1)
};

const configurationLayout = {
    height: process.env.LGV_HEIGHT || configLayout.height,
    width: process.env.LGV_WIDTH || configLayout.width
};

const configurationSeries = {
    labelsOnTop: process.env.LGV_SERIES_LABELS_ON_TOP || configSeries.labelsOnTop,
    normalize: process.env.LGV_SERIES_NORMAILIZE || configSeries.normalize,
    padding: process.env.LGV_SERIES_PADDING || configSeries.padding,
    useLabels: process.env.LGV_SERIES_LABELS || configSeries.useLabels,
};

export { configuration, configurationLayout, configurationSeries };
export default configuration;
